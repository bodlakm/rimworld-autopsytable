﻿using System;

namespace AutopsyTable
{
	public class WorkGiver_Autopsy : WorkGiver_DoBill
	{
		public WorkGiver_Autopsy() : base(LocalJobDefOf.Autopsy, true) {}
	}
}