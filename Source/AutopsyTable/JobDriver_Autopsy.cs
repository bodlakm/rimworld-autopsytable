﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using RimWorld;
using Verse;
using Verse.AI;
using System.Text;

namespace AutopsyTable
{
	public class JobDriver_Autopsy : JobDriver_DoBill
	{
		private List<BodyPartRecord> partsList;
		private List<ThingDef> thingsToSpawn = new List<ThingDef>();
		private int partsCount;
		private int partIndex;
		private Pawn innerPawn;
		private List<Hediff> innerPawnHediffs;
		float onePartWorkAmount;
		float workAmount;
		int workIndex;
		float corpseSize, surgerySuccessChance, infectionChance, corpseAge;
		string tableQuality, tableStuff;

		private float calculateChance(int doctorSkill, float roomSurgery, float roomInfection, float age, bool isBionic, string tableQuality, string tableStuff)
		{
			float chance = 1f;
			float corpseAgeFactor = 1.01f;
			float bionicFactor = 1f;
			if (isBionic) {
				bionicFactor = 1.5f;
			} else {
				if (age > 4 && age < 24) {
					corpseAgeFactor = (24f - age) / 20f + 0.01f;
				} else if (age >= 24) {
					corpseAgeFactor = 0.01f;
				}
				
			}
			float doctorSkillFactor = ((float)doctorSkill + 1f) / 200f;
			float roomSurgeryFactor = roomSurgery;
			float roomInfectionFactor = (1 - roomInfection) + 0.5f;
			float tableQualityFactor = 1f;
			switch (tableQuality) {
			case "awful":
				tableQualityFactor = 0.7f;
				break;
			case "shoddy":
				tableQualityFactor = 0.8f;
				break;
			case "poor":
				tableQualityFactor = 0.9f;
				break;
			case "normal":
				tableQualityFactor = 1f;
				break;
			case "good":
				tableQualityFactor = 1.1f;
				break;
			case "superior":
				tableQualityFactor = 1.2f;
				break;
			case "excellent":
				tableQualityFactor = 1.3f;
				break;
			case "masterwork":
				tableQualityFactor = 1.4f;
				break;
			case "legendary":
				tableQualityFactor = 1.5f;
				break;
			}
			float tableStuffFactor = 1f;
			switch (tableStuff) {
			case "Silver":
				tableStuffFactor = 1.2f;
				break;
			case "Steel":
				tableStuffFactor = 1.1f;
				break;
			case "Wood":
				tableStuffFactor = 0.9f;
				break;
			}
			chance = doctorSkillFactor * roomSurgeryFactor * roomInfectionFactor * corpseAgeFactor * bionicFactor * tableQualityFactor * tableStuffFactor;
			if (Prefs.DevMode) {
				StringBuilder sb = new StringBuilder ();
				sb.Append("Doctor skill: ").Append (doctorSkillFactor.ToString()).AppendLine();
				sb.Append("Room surgery success: ").Append (roomSurgeryFactor.ToString()).AppendLine();
				sb.Append("Room surgery infection: ").Append (roomInfectionFactor.ToString()).AppendLine();
				sb.Append("Corpse age: ").Append (corpseAgeFactor.ToString()).AppendLine();
				sb.Append("Table quality: ").Append (tableQualityFactor.ToString()).AppendLine();
				sb.Append("Table stuff: ").Append (tableStuffFactor.ToString()).AppendLine();
				sb.Append("Bionic part: ").Append (bionicFactor.ToString()).AppendLine();
				sb.Append("Total chance: ").Append (chance.ToString()).AppendLine();
				Log.Message (sb.ToString ());
			}
			return chance;
		}

		protected override Toil DoBill()
		{
			Pawn actor = GetActor ();
			Job curJob = actor.jobs.curJob;
			Corpse objectThing = curJob.GetTarget(objectTI).Thing as Corpse;
			Building_WorkTable tableThing = curJob.GetTarget(tableTI).Thing as Building_WorkTable;

			Toil toil = new Toil ();
			toil.initAction = delegate {
				objectThing.Strip();
				curJob.bill.Notify_DoBillStarted(actor);
				innerPawn = objectThing.InnerPawn;
				innerPawnHediffs = innerPawn.health.hediffSet.hediffs;
				partsList = innerPawn.health.hediffSet.GetNotMissingParts (BodyPartHeight.Undefined, BodyPartDepth.Undefined).ToList ();
				partsCount = partsList.Count;
				partIndex = 0;
				workIndex = 0;
				onePartWorkAmount = curJob.RecipeDef.workAmount / 10f;
				workAmount = onePartWorkAmount * partsCount;
				corpseSize = objectThing.InnerPawn.BodySize;
				int years = 0, quadrums = 0, days = 0;
				float hours = 0f;
				GenDate.TicksToPeriod(objectThing.Age, out years, out quadrums, out days, out hours);
				corpseAge = hours + 24 * days + 15 * 24 * quadrums + 4 * 15 * 24 * years;
				surgerySuccessChance = tableThing.GetRoom(RegionType.Set_Passable).GetStat(RoomStatDefOf.SurgerySuccessChanceFactor);
				infectionChance = tableThing.GetRoom(RegionType.Set_Passable).GetStat(RoomStatDefOf.InfectionChanceFactor);
				//
				QualityCategory qc;
				tableThing.TryGetQuality(out qc);
				tableQuality = qc.GetLabel();
				tableStuff = tableThing.Stuff.LabelCap;
			};
			toil.tickAction = delegate {
				if (objectThing == null || objectThing.Destroyed) {
					actor.jobs.EndCurrentJob(JobCondition.Incompletable);
				}
				tableThing.UsedThisTick ();
				if (!tableThing.UsableNow) {
					actor.jobs.EndCurrentJob (JobCondition.Incompletable);
				}
				SkillDef skillDef = curJob.RecipeDef.workSkill;
				int skillLevel = 0;
				if (skillDef != null) {
					SkillRecord skill = actor.skills.GetSkill (skillDef);
					if (skill != null) {
						skillLevel = skill.Level;
						skill.Learn (0.5f * curJob.RecipeDef.workSkillLearnFactor);
					}
				}
				if (workIndex < workAmount) {
					int newPartIndex = (int) Math.Floor(workIndex / onePartWorkAmount);
					if (newPartIndex > partIndex) {
						partIndex = newPartIndex;
						BodyPartRecord currentPart = partsList[partIndex];
						ThingDef thingToSpawn = null;
						bool isBionic = false;
						if (innerPawn.health.hediffSet.HasDirectlyAddedPartFor(currentPart)) {
							foreach (Hediff h in innerPawnHediffs) {
								if (h is Hediff_AddedPart) {
									Hediff_AddedPart ha = (Hediff_AddedPart)h;
									if (ha.Part == currentPart && ha.def.spawnThingOnRemoved != null) {
										isBionic = true;
										thingToSpawn = ha.def.spawnThingOnRemoved;
									}
								}
							}
						} else if (currentPart.def.spawnThingOnRemoved != null) {
							thingToSpawn = currentPart.def.spawnThingOnRemoved;
						}
							
						if (thingToSpawn != null) {
							float chance = this.calculateChance(skillLevel, surgerySuccessChance, infectionChance, corpseAge, isBionic, tableQuality, tableStuff);
							float rnd = UnityEngine.Random.value;
							if (rnd < chance) {
								thingsToSpawn.Add(thingToSpawn);
							}
						}
					}
					workIndex++;
				} else {
					foreach (ThingDef t in thingsToSpawn) {
						GenSpawn.Spawn (t, actor.Position, actor.Map);
					}
					//StringBuilder sb = new StringBuilder();
					foreach (Thing t in innerPawn.ButcherProducts(actor, 0.3f).ToList()) {
						GenSpawn.Spawn (t, actor.Position, actor.Map);
					}
					ThoughtDef thdef;
					if (innerPawn.IsColonist) {
						thdef = ThoughtDefOf.KnowButcheredHumanlikeCorpse;
					} else {
						thdef = ThoughtDefOf.ButcheredHumanlikeCorpse;
					}
					foreach (Pawn current in from x in PawnsFinder.AllMapsCaravansAndTravelingTransportPods
						where x.IsColonist || x.IsPrisonerOfColony
						select x) {
						current.needs.mood.thoughts.memories.TryGainMemory (thdef, null);
					}
					curJob.bill.Notify_IterationCompleted(actor, new List<Thing>());
					pawn.Map.reservationManager.Release(curJob.targetB, pawn);
					objectThing.Destroy(DestroyMode.Vanish);
					ReadyForNextToil();
				}
			};

			toil.defaultCompleteMode = ToilCompleteMode.Never;
			toil.WithEffect (() => curJob.bill.recipe.effectWorking, tableTI);
			toil.PlaySustainerOrSound (() => toil.actor.CurJob.bill.recipe.soundWorking);
			toil.FailOn (() => {
				return toil.actor.CurJob.bill.suspended || !tableThing.UsableNow;
			});
			return toil;
		}
	}
}

