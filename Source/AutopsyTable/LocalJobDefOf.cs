﻿using System;

using Verse;
using RimWorld;

namespace AutopsyTable
{
	[DefOf]
	public static class LocalJobDefOf {
		public static JobDef Autopsy;
	}

}